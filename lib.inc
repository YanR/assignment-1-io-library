section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, rsp
    sub rsp, 32
    mov rax, rdi
    dec r9
    mov byte [r9], 0x0
    mov rcx, 10
.loop:
    xor rdx, rdx
    div rcx
    add rdx, '0'
    dec r9
    mov [r9], dl
    test rax, rax
    je .end
    jmp .loop
.end:
    mov rdi, r9
    call print_string
    add rsp, 32
    ret
    
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax   
    xor rcx, rcx        
.loop:
    mov al, byte[rsi+rcx]   
    mov dl, byte[rdi+rcx]  
    cmp al, dl 
    jne .not_equal  
    cmp al, 0
    je .equal  
    inc rcx     
    jmp .loop
.equal:
    mov rax, 1      
    ret
.not_equal:
    xor rax, rax  
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    lea rsi, [rsp-1]
    mov rdx, 1
    syscall
    test rax, rax
    jz .end
    mov al, byte[rsp-1]
.end:
    ret
    
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .loop:
        call read_char
        test rax, rax
        je .end
        cmp rax, ' '
        je .space
        cmp rax, `\n`
        je .space
        cmp rax, `\t`
        je .space
        mov  byte[r12+r14], al
        inc r14
        cmp r14, r13 
        jge .overflow
        jmp .loop
    .space:
        test r14, r14
        jz .loop
    .end:
        mov byte[r12+r14], 0
        mov rax, r12
        mov rdx, r14
        jmp .exit
    .overflow:
        xor rax, rax
    .exit:
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi
    movzx rsi, byte [rdi]
    cmp rsi, '0'
    jl .not_a_digit
    cmp rsi, '9'
    jg .not_a_digit
.loop:
    sub rsi, '0'
    imul rax, rax, 10
    add rax, rsi
    inc rdx
    movzx rsi, byte [rcx+rdx]
    cmp rsi, '0'
    jl .done
    cmp rsi, '9'
    jg .done
    jmp .loop
.not_a_digit:
    xor rdx, rdx
.done:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    jae .overflow
    mov r8b, byte[rdi+rax]
    mov [rsi+rax], r8b
    test r8b, r8b 
    jz .end       
    inc rax    
    jmp .loop        
.overflow:
    xor rax, rax
.end:
    ret
